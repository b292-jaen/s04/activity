
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO users(username, password, full_name, contact_number, email, address) VALUES 
("ezrealAD", "passwordA", "Jarro LightFeather", 214734223, "ez@gmail.com", "Piltover"), 
("seraphine", "passwordB", "Seraphine", 214734223, "seraphine@gmail.com", "Iona"), 
("lux" , "passwordC", "Luxanna Crownguard", 214734223 ,"lux@gmail.com", "Demacia" ), 
("viktor", "passwordD", "Viktor", 214734223, "viktor@gmail.com", "Zaun" ), 
("jinx", "passwordE", "Jinx", 214734223,  "jinx@gmail.com", "Zaun");

CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	review VARCHAR(500) NOT NULL,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_reviews_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

INSERT INTO reviews(review, user_id, rating, datetime_created) VALUES 
("Twice is mid, blackpink better", 1 , 2, "2023-05-03"),
("My favorite is HYLT!", 2,  5, "2023-05-03"),
("I LOVE BTS!", 3,  5, "2023-05-03"),
("Rythm is repetitive", 4,  1, "2023-05-03"),
("SVT Fighting!",5,  5, "2023-05-03");

SELECT * FROM users 
	LEFT JOIN reviews ON users.id = reviews.user_id
	WHERE full_name LIKE "%k%";

SELECT * FROM users 
	LEFT JOIN reviews ON users.id = reviews.user_id
	WHERE full_name LIKE "%x%";

SELECT * FROM users 
	JOIN reviews ON users.id = reviews.user_id;

SELECT users.username, reviews.review FROM users 
	JOIN reviews ON users.id = reviews.user_id;
